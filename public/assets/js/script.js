//loops are structures that allow us to execute code repeatedly
/*
	
	there are three types of loops:
	1. While loop
	2. Do-While loop
	3. For loop

*/

//while loop - executes as long as the condition is satisfied
/*
while(condition to check){
	//code here
	//increment or decrement
}
*/

let count = 1; //initial statement
while (count <= 10) { //condition for the loop to work
	console.log("the current count is: " + count); //do anything inside the loop
	count++; //increment -> makes sure that the value of count changes and reaches 10
}

//do while loop - executes as long as the condition is satisfied and functions almost like a while loop exept it makes sure that the loop runs at least once.

let count2 = 10;
do {
	console.log(count2);
	count2--;
} while (count2 >= 0);

//this is how a do while runs at least once:
let count3 = 1;
do {
	console.log("hello world");
	count3++;
} while (count3 <= 0);


// the main difference of a while and do whilte can be summarized into this:
/* 
while loop check first before executing (check before gawa)
do while loop executes first before checking (gawa before check)
*/


//for loop is a loop that has 3 parts, the initial value, the condition and the iteration (increment/decrement)
/*
for initial value; condition; increment/decrement){
	//code here
}
*/

 for(let i = 0; i < 5; i++) {
 	console.log("The current count is " + i);
 	console.log("The ccurent count * 2 is " + (i * 2));
 }

 //mini exercise:
 /*
Use any loop (while, do while, for) to display all the numbers from 1 to 10 and say if it is odd or even

output:
1 -> odd
2 -> even
3 -> odd
...
10 -> even
 */

 for(let i = 1; i <= 10; i++) {
 	if (i % 2 == 0){
 		console.log(i + " -> even");
 	} else {
 		console.log(i + " -> odd");
 	}
 }


//continue is a keyword that allows the loop to skip the current count and proceed to the next
// break automatically stops the loop

for(let count = 1; count <= 10; count++){
	if (count == 5) {
		continue;
	}
	console.log(count);
}

for(let count = 1; count <= 10; count++){
	if (count == 5) {
		break;
	}
	console.log(count);
}

//difference between +aa and a++; (preincrement vs postincrement)
let c = 1;
let d = 1;
let e =  c++; // 1
let f = ++d;  // 2

console.log(e);
console.log(f);